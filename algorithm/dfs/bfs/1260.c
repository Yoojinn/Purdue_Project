#include <iostream>
#include <stdio.h>
#include <queue>

using namespace std;

typedef struct point{
	int x;
	int y;
}point;

int n, m, v;
int arr[1000][1000] = {0,};
int check_s[10000] = {0,};
int check_q[10000] = {0,};

queue<point> q;

void dfs(int x, int y)
{
	if(check_s[y] == 1)
	{
		return;
	}
	else
		printf("%d ", y+1);
		
	for(int i = 0; i < n; i++)
	{
		if(arr[y][i] == 1)
		{
			check_s[y] = 1;
			dfs(y, i);
		}
	}
}

void bfs(int x, int y)
{
	point p;
	p.x = y;
	
	if(check_q[y] == 1)
	{
		return;
	}
	else
		printf("%d ", y+1);
	for(int i = 0; i < n; i++)
	{
		if(arr[y][i] == 1)
		{
			p.y = i;
			check_q[y] = 1;
			q.push(p);
		}
	}
}

int main() {
	scanf("%d %d %d", &n, &m, &v);
	check_s[v-1] = 1;
	check_q[v-1] = 1;
	for(int i = 0; i < m; i++)
	{
		int tmp1, tmp2;
		scanf("%d %d", &tmp1, &tmp2);
		arr[tmp1-1][tmp2-1] = 1;
		arr[tmp2-1][tmp1-1] = 1;
	}
	
	printf("%d ", v);
	for(int i = 0; i < n; i++)
	{
		if(arr[v-1][i] == 1)
			dfs(v-1, i);
	}
	printf("\n");
	printf("%d ", v);
	
	point p;
	p.x = v-1;
	
	for(int i = 0; i < n; i++)
	{
		if(arr[v-1][i] == 1)
		{
			p.y = i;
			q.push(p);
		}
	}
	while(q.size() != 0)
	{
		bfs(q.front().x, q.front().y);
		q.pop();
	}
	
}