#include <iostream>
#include <stdio.h>
#include <queue>

using namespace std;

struct point{
	int x;
	int y;
	int dist;
};

int m, n;
int arr[101][101] = {0,};
int dist[101] = {0,};
queue<point> q;



void bfs(int x, int y, int cnt)
{
	struct point p;

	
	if(x+1 < n && arr[x+1][y] == 1)
	{
		p.x = x+1;
		p.y = y;
		p.dist = cnt+1;
		q.push(p);
		arr[x+1][y] = 0;
	
	}
	if(x-1 >= 0 && arr[x-1][y] == 1)
	{
		p.x = x-1;
		p.y = y;
		p.dist = cnt+1;
		q.push(p);
		arr[x-1][y] = 0;
		
	}
	if(y+1 < m && arr[x][y+1] == 1)
	{
		p.x = x;
		p.y = y+1;
		p.dist = cnt+1;
		q.push(p);
		arr[x][y+1] = 0;
		
	}
	if(y-1 >= 0 && arr[x][y-1] == 1)
	{
		p.x = x;
		p.y = y-1;
		p.dist = cnt+1;
		q.push(p);
		arr[x][y-1] = 0;
		
	}
	
}
int main() {
	struct point temp;
	scanf("%d %d", &n, &m);

	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < m; j++)
		{
			scanf("%1d", &arr[i][j]);
		}
	}
	
	temp.x = 0;
	temp.y = 0;
	temp.dist = 0;

	q.push(temp);
	
	while(1)
	{
		//printf("%d %d %d\n",q.front().x,q.front().y,q.front().dist);
		if(q.front().x == n-1 && q.front().y == m-1)
			break;
		//printf("%d %d %d\n",q.front().x,q.front().y,q.front().dist);
		bfs(q.front().x,q.front().y,q.front().dist);
		q.pop();
	}
	printf("%d\n", q.front().dist+1);
	
}