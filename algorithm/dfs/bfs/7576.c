#include <iostream>
#include <stdio.h>
#include <queue>

using namespace std;

struct point{
	int x;
	int y;
	int day;
};

int arr[1000][1000] = {0,}; 
int map[1000][1000] = {0,};
queue<point> q;
int m = 0, n = 0;
int cnt = 0;

void bfs(int x, int y, int num)
{
	struct point temp;
	
	map[x][y] = num+1;
	if(x+1 < m && arr[x+1][y] == 0 && map[x+1][y] == 0)
	{
		map[x+1][y] = num+1;
		temp.x = x+1;
		temp.y = y;
		temp.day = num+1;
		q.push(temp);
	}
	if(x-1 >= 0 && arr[x-1][y] == 0 && map[x-1][y] == 0)
	{
		map[x-1][y] = num+1;
		temp.x = x-1;
		temp.y = y;
		temp.day = num+1;
		q.push(temp);
	}
	if(y+1 < n && arr[x][y+1] == 0 && map[x][y+1] == 0)
	{
		map[x][y+1] = num+1;
		temp.x = x;
		temp.y = y+1;
		temp.day =num+1;
		q.push(temp);
	}
	if(y-1 >= 0 && arr[x][y-1] == 0 && map[x][y-1] == 0)
	{
		map[x][y-1] = num+1;
		temp.x = x;
		temp.y = y-1;
		temp.day = num+1;
		q.push(temp);
	}
	
}
int main()
{
	
	
	scanf("%d %d", &n, &m);
	
	for(int i = 0; i < m; i++)
	{
		for(int j = 0; j < n; j++)
		{
			scanf("%d", &arr[i][j]);
			//map[i][j] = arr[i][j];
			if(arr[i][j] == 1)
			{
				struct point temp;
				temp.x = i;
				temp.y = j;
				temp.day = 0;
				//printf("%d %d\n", i, j);
				q.push(temp);
				
			}
			if(arr[i][j] == -1)
			{
				map[i][j] = -1;
			}
		}
	}
	
	while(q.size() != 0)
	{
		bfs(q.front().x, q.front().y, q.front().day);
		q.pop();
	}
	int max = map[0][0];
	/*
	for(int i = 0; i < m; i++)
	{
		for(int j = 0; j < n; j++)
		{
			printf("%d ", map[i][j]);
		}
		printf("\n");
	}
	*/
	for(int i = 0; i < m; i++)
	{
		for(int j = 0; j < n; j++)
		{
		
			if(max < map[i][j])
			{
				max = map[i][j];
			}
			if(map[i][j] == 0)
			{
				max = -1;
				printf("%d\n", -1);
				return 0;
			}
		}
	}
	
	printf("%d\n", max-1);
}